# Prerequisites
- ARKit kompatibles device mit iOS12
- macOS 10.13+
- XCode 10+

# hilfreich (weil kein BUild erforderlich zum Testen), aber scheiß Performance: Unity ARKit Remote
- Remote für gewünschtes Device compilen (ist zB. am iPad Pro schon drauf): UnityARKitPlugin → ARKitRemote → UnityARKitRemote.unity builden und deployen
- UnityARKitRemote auf device launchen
- in Unity (zu testende Szene): leeres GameObject erzeugen → Add Component → `ARKitRemoteConnection` script adden
- in Unity: Play → in Console gewünschte Remote auswählen → Button erscheint: "Start Remote ARKit Session"

# Unity ARKit Image Tracking Sample
- verwendete Szene: Assets/UnityARKit/Examples/ARKit1.5/UnityARImageAnchor/UnityARImageAnchor.unity
- gewünschtes Target in Unity adden
- pro Bild neues `ARReferenceImage` anlegen (Create → UnityARKitPlugin → ARReferenceImage)
- neues `ARReferenceIamgeSet` erstellen (Create → UnityARKitPlugin → ARReferenceImageSet) und alle `ARReferenceIamges` adden
- für Anzeige: pro Image Target neues, leeres GameObject erzeugen → Add Component → `GenerateImageAnchor` script adden → "PreFab To Generate" mit gewünschtem PreFab/Model/Gameobject belegen
- builden

# Unity ARKit Plane Detection Sample
- verwendete Szene: Assets/UnityARKit/Examples/UnityARKitScene/UnityARKitScene.unity

- neue Textur erstellen/hinzufügen (RasterGray.png; habe auch noch ein 9-sprite-des Rasters für die Plane versuchen anzuzeigen, hat aber nicht funktioniert)
- neues Material erstellen → Textur einfügen
- im debugPlanePrefab bzw. dessen Child "Plane" im Mesh Renderer durch eigenes Material ersetzen