﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR.iOS;

public class CustomAnchor{
	public CustomAnchor(GameObject go, int num){
		anchorGO = go;
		updateCnt = 0;
		maxUpdates = num;
	}
	private int maxUpdates, updateCnt;
	Vector3 avgPosition, avgRotation;
	//UnityARUserAnchorComponent anchorComponent;
	GameObject anchorGO;
	public bool updatePositionAndRotation(Vector3 newPos, Quaternion newRotation){
		if(updateCnt == 0){
			avgPosition = newPos;
			avgRotation = newRotation.eulerAngles;
			updateCnt++;
			return true;
		} else if(updateCnt >= maxUpdates){
			return false;
		} else {
			// update position to new position
			// this is NOT mean -> each new position contributes with the same strength as sum of existing positions
			avgPosition += newPos;
			avgPosition /= 2;
			Debug.LogFormat("new position at {0}/{1}/{2}", avgPosition.x, avgPosition.y, avgPosition.z);

			// ... same for rotation
			avgRotation += newRotation.eulerAngles;
			avgRotation /= 2;
			Debug.LogFormat("new rotation {0}/{1}/{2}", avgRotation.x, avgRotation.y, avgRotation.z);

			updateCnt++;

			anchorGO.transform.position = avgPosition;
			anchorGO.transform.rotation = Quaternion.Euler(avgRotation);
			return true;
		}
	}

	public int getUpdateCnt(){
		return updateCnt;
	}
}
public class GenerateImageAnchor : MonoBehaviour {

	[SerializeField]
	private ARReferenceImage referenceImage;

	[SerializeField]
	private GameObject prefabToGenerate;

	private Hashtable anchorTable;

	int numAnchorUpdates = 50;

	// Use this for initialization
	void Start () {
		UnityARSessionNativeInterface.ARImageAnchorAddedEvent += AddImageAnchor;
		UnityARSessionNativeInterface.ARImageAnchorUpdatedEvent += UpdateImageAnchor;
		UnityARSessionNativeInterface.ARImageAnchorRemovedEvent += RemoveImageAnchor;
		UnityARSessionNativeInterface.ARUserAnchorAddedEvent += UserAnchorAdded;

		anchorTable = new Hashtable();
	}

	void AddImageAnchor(ARImageAnchor arImageAnchor)
	{
		Debug.LogFormat("image anchor added[{0}] : tracked => {1}", arImageAnchor.identifier, arImageAnchor.isTracked);
		if (arImageAnchor.referenceImageName == referenceImage.imageName) {
			Vector3 position = UnityARMatrixOps.GetPosition (arImageAnchor.transform);
			Quaternion rotation = UnityARMatrixOps.GetRotation (arImageAnchor.transform);

			//imageAnchorGO = Instantiate<GameObject> (prefabToGenerate, position, rotation);

			if(!anchorTable.Contains(arImageAnchor.referenceImageName)){
				GameObject go = Instantiate<GameObject>(prefabToGenerate, position, rotation);
				UnityARUserAnchorComponent component = go.GetComponent<UnityARUserAnchorComponent>();
				anchorTable.Add(arImageAnchor.referenceImageName, new CustomAnchor(go, numAnchorUpdates));
			}
		}
	}

	void UpdateImageAnchor(ARImageAnchor arImageAnchor)
	{
		// Debug.LogFormat("image anchor updated[{0}] : tracked => {1}", arImageAnchor.identifier, arImageAnchor.isTracked);
		if (arImageAnchor.referenceImageName == referenceImage.imageName && anchorTable.Contains(arImageAnchor.referenceImageName)){
			CustomAnchor ca = (CustomAnchor) anchorTable[arImageAnchor.referenceImageName];
			bool didUpdate = ca.updatePositionAndRotation(UnityARMatrixOps.GetPosition(arImageAnchor.transform), UnityARMatrixOps.GetRotation(arImageAnchor.transform));
			if(!didUpdate){
				Debug.LogFormat("{0} updates until now", ca.getUpdateCnt());
			} else {
				Debug.LogFormat("didn't update anymore, already {0} updates", ca.getUpdateCnt());
			}
        }
	}

	void RemoveImageAnchor(ARImageAnchor arImageAnchor)
	{
		/*
		Debug.LogFormat("image anchor removed[{0}] : tracked => {1}", arImageAnchor.identifier, arImageAnchor.isTracked);
		
		if (imageAnchorGO) {
			GameObject.Destroy (imageAnchorGO);
		}
		*/
	}

	void UserAnchorAdded(ARUserAnchor userAnchor){
		Debug.LogFormat("userAnchor added: {0}", userAnchor.identifier);
	}

	void OnDestroy()
	{
		UnityARSessionNativeInterface.ARImageAnchorAddedEvent -= AddImageAnchor;
		UnityARSessionNativeInterface.ARImageAnchorUpdatedEvent -= UpdateImageAnchor;
		UnityARSessionNativeInterface.ARImageAnchorRemovedEvent -= RemoveImageAnchor;
		UnityARSessionNativeInterface.ARUserAnchorAddedEvent -= UserAnchorAdded;
	}

	// Update is called once per frame
	void Update () {
		
	}
}
